#!/usr/bin/python
# coding: utf-8

import datetime
import sys  # sys.stdout.write('.')
import traceback
import validators
import os
import csv
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from bs4 import BeautifulSoup
from HTMLParser import HTMLParser
from selenium.webdriver.support import expected_conditions as EC

date = datetime.datetime.now().strftime("%Y-%m-%d-%H:%M")
filename = 'krakowbackup-' + date + '.xml'
backupfile = open("backups/"+filename, 'w')
version = '0.0'

backupfile.write("<?xml version='1.0' encoding='UTF-8'?>\n<script version='"+version+"'/>\n<backup date='"+date+"'/>\n<forum title='Легенды Кракова'>")

# Disable image, css and flash loading
firefox_profile = webdriver.FirefoxProfile()
firefox_profile.set_preference('permissions.default.stylesheet', 2)
firefox_profile.set_preference('permissions.default.image', 2)
firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')
#browser setup
fox = webdriver.Firefox(firefox_profile)

path = os.path.abspath('data.csv') #path to login data

#iteration variables
subforumid = 0 # subforum counter
topicid = 0 # topic counter
postid = 0 # posts counter

################################################################################

def login():  # function to log in as backup
    print '\tЗагрузка...'
    fox.get('http://holerajasna.liverolka.ru/login.php')
    usr = ''
    passwd = ''
    with open(path, 'rU') as data:
        reader = csv.DictReader(data)
        for row in reader:
            usr = row['usr']
            passwd = row['pass']
    username = fox.find_element_by_xpath('//input[@size = "25"]')
    password = fox.find_element_by_xpath('//input[@size = "16"]')
    print '\tВвод пользовательских данных...'
    username.send_keys(usr.decode('utf-8'))
    password.send_keys(passwd.decode('utf-8'), Keys.RETURN)
    fox.implicitly_wait(3)  # important! Wait for 10 seconds to load
    print '\tВход произведён\n'

#### Helpful functions #########################################################
class clearAuthor(HTMLParser):
    def handle_data(self, data):
        if data != 'Автор:':
            self.data = data
clearauthor = clearAuthor()

def geturl(line):  # simple url searcher
    res = line.split('"')
    for r in res:
        if validators.url(r):
            return r
    return ''

def getavatar(author):
    xpath = '//img[@alt="'+str(author)+'"]'
    try:
        avatar = fox.find_element_by_xpath(xpath)
        # clearurl.feed(avatar.get_attribute('outerHTML'))
        url = geturl(avatar.get_attribute('outerHTML'))
        # print avatar.get_attribute('outerHTML')
        backupfile.write("\n\t\t\t\t<avatar>"+url+"</avatar>")
    except NoSuchElementException:
        # print 'user ', str(author), ' has no avatar'
        backupfile.write("\n\t\t\t\t<avatar> </avatar>")
        return False
    return True

def nextpage():  # searches for nextpage link and clicks if found
    try:
        np = fox.find_element_by_xpath('//a[@class="next"]')
        np.click()
        fox.implicitly_wait(3)
    except NoSuchElementException:
        # print 'no next page'
        return False
    # print u'=>\tСледующая страница'
    # print np.get_attribute('outerHTML')
    return True

#### Main functionality ########################################################

# makes a list of subforums
def subforum_handler():
    subforum_urls = []  # use to stash urls of subforums
    subforum_titles = [] # stash all titles
    subforums = fox.find_elements_by_xpath('//h3//a') # find all elements

    for s in subforums:
        surl = geturl(s.get_attribute('outerHTML').encode('utf-8'))
        stitle = s.get_attribute('innerHTML').encode('utf-8')
        if validators.url(surl):
            subforum_urls.append(surl)
            subforum_titles.append(stitle)

    i = 0
    for i in range(len(subforum_urls)):
        global subforumid
        subforumid = subforumid + 1
        fox.get(subforum_urls[i])
        fox.implicitly_wait(3)
        print u'\tВ РАЗДЕЛЕ:'
        print '\t\t', subforum_titles[i], ' : ', subforum_urls[i]
        fline = "\n\t"+"<subforum id='"+str(subforumid)+"' title='"+subforum_titles[i]+"'>\n\t\t<title>"+subforum_titles[i]+"</title>"
        backupfile.write(fline)
        topic_handler()
        backupfile.write("\n\t</subforum>")

# finds all topics, goes onto any next page.
# Called by subforum_handler()
def topic_handler():
    lastpage = fox.current_url
    topic_urls = []
    topic_titles = []
    # get topics (use outerHTML)
    topics = fox.find_elements_by_xpath('//div[@class="tclcon"]/a')

    for t in topics:
        turl = geturl(t.get_attribute('outerHTML').encode('utf-8'))
        ttitle = t.get_attribute('innerHTML').encode('utf-8')
        # print ttitle.strip('<span class="acchide">Тема&nbsp;1&nbsp;Page&nbsp;</span')
        if validators.url(turl) and ('span' or 'Реклама v') not in ttitle:
            topic_urls.append(turl)
            topic_titles.append(ttitle)

    i = 0
    for i in range(len(topic_urls)):
        global topicid
        topicid = topicid + 1
        fox.get(topic_urls[i])
        fox.implicitly_wait(3)
        print u'\t\tВ ТЕМЕ:'
        print '\t\t\t', topic_titles[i], ' : ', topic_urls[i]
        backupfile.write("\n\t\t<topic id='"+str(topicid)+"' title='"+topic_titles[i]+"'>\n\t\t\t<title>"+topic_titles[i]+"</title>")
        posts_handler()
        backupfile.write("\n\t\t</topic>")

    fox.get(lastpage) # go back to last parsed topic page
    if nextpage(): # then go to the next one
        topic_handler()

def posts_handler():
    ###
    # Handles posts in topic separately
    ###
    # print 'run posts_handler()'
    #topic_title = fox.find_element_by_tag_name('title').get_attribute('innerHTML').encode('utf-8')
    authors = fox.find_elements_by_xpath('//li[@class="pa-author"]')
    #avatars = fox.find_elements_by_xpath('//img[@alt]')
    author_titles = fox.find_elements_by_xpath('//li[@class="pa-title"]')
    posts_content = fox.find_elements_by_xpath('//div[@class="post-content"]')
    author_signatures = fox.find_elements_by_xpath('//dl[@class="post-sig"]//p')

    # backupfile.write("\n\t\t<topic title='"+topic_title+"'>\n\t\t\t<title>"+topic_title+"</title>")
    # print '<topic>'
    # sys.stdout.write('\t<title>'+topic_title.get_attribute('innerHTML').encode('utf-8')+'</title>\n')

    i = 0
    for i in range(len(authors)):
        global postid
        postid = postid + 1

        clearauthor.feed(authors[i].get_attribute('outerHTML').encode('utf-8'))
        author = clearauthor.data.strip()
        # print "\t\t\t\tПост: "+str(postid)+", автор: "+author
        backupfile.write("\n\t\t\t<post id='"+str(postid)+"'>")
        backupfile.write("\n\t\t\t\t<author>"+author+"</author>")
        backupfile.write("\n\t\t\t\t<author_title>"+author_titles[i].get_attribute('innerHTML').encode('utf-8')+"</author_title>")
        getavatar(clearauthor.data)
        # backupfile.write("\n\t\t\t\t<content>"+posts_content[i].get_attribute('innerHTML').encode('utf-8').strip()+"</content>")
        soup = BeautifulSoup(posts_content[i].get_attribute('innerHTML').encode('utf-8').strip(), 'html.parser')

        backupfile.write("\n\t\t\t\t<content>"+str(soup).strip()+"</content>")
        # print soup
        # backupfile.write("\n\t\t\t\t<content>")
        # cleartext.feed(str(soup))
        # backupfile.write("</content>")

        if soup.dd != None:
            backupfile.write("\n\t\t\t\t<signature>"+str(soup.dd)+"</signature>")
        else:
            backupfile.write("\n\t\t\t\t<signature> </signature>")

        # sys.stdout.write('\t\t\t<post id="'+str(postid)+'">\n')
        # sys.stdout.write('\t\t\t\t<author>'+clearauthor.data+'</author>\n')
        # sys.stdout.write('\t\t\t\t<author_title>'+author_titles[i].get_attribute('innerHTML').encode('utf-8')+'</author_title>\n')
        # sys.stdout.write('\t\t<content>'+posts_content[i].get_attribute('innerHTML').encode('utf-8')+'</content>\n')
        # sys.stdout.write('\t\t<signature>'+author_signatures[i].get_attribute('innerHTML').encode('utf-8')+'</signature>\n')
        # print '\t</post>'
        backupfile.write("\n\t\t\t</post>")
        # print '\t\tПост : ', postid
    if nextpage():
        posts_handler()

################################################################################

def main():
    try:
        starttime = datetime.datetime.now()
        print "\t" + starttime.strftime("%Y-%m-%d %H:%M"), " : бэкап старт."
        print '\tПишу файл : ', filename
        print '\n\t\t\t===== КРАКОВ БЭКАП =====\n'

        login()
        subforum_handler()
        fox.quit()

        backupfile.write("\n</forum>\n")
        backupfile.close()

        endtime = datetime.datetime.now()
        total = endtime - starttime
        s = total.seconds
        print '\t', endtime.strftime("%Y-%m-%d %H:%M"), " : бэкап завершён."
        print "\tПроцесс занял: {:02} часов {:02} минут {:02} секунд".format(s// 3600, s % 3600 // 60, s % 60)
        print "\tВсего разделов: ", subforumid
        print "\tВсего тем: ", topicid
        print "\tВсего сообщений: ", postid
    except KeyboardInterrupt:
        print "Shutdown requested...exiting"
    except Exception:
        traceback.print_exc(file=sys.stdout)
    sys.exit(0)

if __name__ == "__main__":
    main()
